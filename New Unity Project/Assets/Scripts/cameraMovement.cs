﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMovement : MonoBehaviour
{
    Vector3 startPos;
    Vector3 endPos;
    Vector2 moveDir;

    public GameObject mainCameraObject;
    public Rigidbody2D playerRigid;
    public GameObject playerObject;

    private bool startLerp = false;

    manController manScript;

    // Start is called before the first frame update
    void Start()
    {
        startPos = mainCameraObject.transform.localPosition;
        endPos = startPos + new Vector3(0, 9.5f, 0);
        manScript = playerObject.GetComponent<manController>();
        //Debug.Log(startPos);
    }

    // Update is called once per frame
    void Update()
    {

        if (startLerp == true)
        {
            mainCameraObject.transform.position = Vector3.MoveTowards(mainCameraObject.transform.position, endPos, .076f);
        }

        if (mainCameraObject.transform.position == endPos)
        {
            endPos += new Vector3(0f, 9.5f, 0f);
            playerRigid.constraints = RigidbodyConstraints2D.None;
            Debug.Log("Player is unfrozen");
            startLerp = false;
            //trying to save the force so when the camera transitions I can carry it over
            // maybe just apply a predetermined force if I give up on this, make it slightly random
            //Debug.Log(moveDir);
            //playerRigid.AddForce(moveDir * 15f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            startLerp = true;
            playerRigid.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

}
