﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class O2Pills : MonoBehaviour
{


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            OxygenBar.oxygen += 20;
            Destroy(this.gameObject);
        }
    }

}
