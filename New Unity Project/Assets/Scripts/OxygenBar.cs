﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OxygenBar : MonoBehaviour
{
    Image oxyBar;
    float maxOxy = 100f;
    public static float oxygen;
    // Start is called before the first frame update
    void Start()
    {
        oxyBar = GetComponent<Image>();
        oxygen = 0;
    }

    private void Update()
    {
        oxyBar.fillAmount = oxygen / maxOxy;
        Debug.Log(oxygen);

    }


}
