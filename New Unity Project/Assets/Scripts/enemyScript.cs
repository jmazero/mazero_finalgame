﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyScript : MonoBehaviour
{

    public float pullForce = 1;
    private bool playerEntered = false;

    public GameObject playerObject;
    public Rigidbody2D playerRigid;

    Vector2 playerPos;
    Vector2 enemyPos;
    //the angle at which we want the player to gravitate along
    Vector2 gravitateAngle;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void FixedUpdate()
    {
        playerPos = playerObject.transform.position;
        enemyPos = this.transform.position;
        gravitateAngle = enemyPos - playerPos;
        gravitateAngle = gravitateAngle.normalized;

        if (playerEntered == true)
        {
            Debug.Log("Applying force towards red face.");
            playerRigid.AddForce(gravitateAngle * pullForce);      
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerEntered = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerEntered = false;
        }
    }

}
