﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manController : MonoBehaviour
{
    public Rigidbody2D manRigid;
    public float clickForce;
    public float jumpForce;

    public Vector2 moveDir;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        Vector2 mousePos = Input.mousePosition;
        //converts mousePos to world units instead of screen pixel units
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(mousePos);
        //dont want the z, so create new vector2 with just the x and y from worldPos
        Vector2 moveToPos = new Vector2(worldPos.x, worldPos.y);
        //get player position
        Vector2 playerPos = new Vector2(this.transform.position.x, this.transform.position.y);

        //get direction between the player's position and where they clicked
        moveDir = moveToPos - playerPos;


        if (Input.GetButtonDown("Fire1"))
        {
            if (OxygenBar.oxygen <= 0)
            {
                //manRigid.constraints = RigidbodyConstraints2D.FreezeAll;
                Debug.Log("Not enough O2");

            }
            else {
                manRigid.constraints = RigidbodyConstraints2D.None;

                moveDir = moveDir.normalized;

                manRigid.AddForce(moveDir * clickForce);

                //subtract oxygen
                OxygenBar.oxygen -= 10f;


                //transform.position = moveToPos;
                Debug.Log(moveDir);
            }


        }

        //make player gameobject look at the position of the mouse
        var dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (manRigid.constraints == RigidbodyConstraints2D.FreezeAll)
            {
                manRigid.constraints = RigidbodyConstraints2D.None;

                moveDir = moveDir.normalized;
                manRigid.AddForce(moveDir * jumpForce);
            }
        }



    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Asteroid"))
        {
            //make player get stuck on the asteroid
            manRigid.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

}
